export interface GitRepositoryBranches {
  local: string;
  remote: string | null;
}
