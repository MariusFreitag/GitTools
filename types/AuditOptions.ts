export interface AuditOptions {
  isParent: boolean;
  interactive: boolean;
  onlyName: string;
  allowGithubAsCommitter: boolean;
  expectConfiguredName: string;
  expectConfiguredEmail: string;
  expectConfiguredLocalBranch: string;
  expectConfiguredRemoteBranch: string;
  expectDefaultMode: boolean;
  expectSync: boolean;
  expectCleanliness: boolean;
  expectAuthorName: string;
  expectAuthorEmail: string;
  expectAuthorCommitterEquality: boolean;
}
