export interface FileChange {
  filename: string;
  changedLines: number;
  additionPortion: number;
  removalPortion: number;
  isBinary: boolean;
}
