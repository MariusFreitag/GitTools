export interface GitCommit {
  hash: string;
  shortHash: string;
  tree: string;
  shortTree: string;
  parent: string;
  shortParent: string;
  refs: string;
  encoding: string;
  subject: string;
  sanitizedSubjectLine: string;
  body: string;
  commitNotes: string;
  verificationFlag: string;
  signer: string;
  signerKey: string;
  author: {
    name: string;
    email: string;
    date: string;
  },
  committer: {
    name: string;
    email: string;
    date: string;
  }
}
