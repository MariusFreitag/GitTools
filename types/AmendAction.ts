export enum AmendAction {
  Nothing,
  Change,
  Squash,
  Remove
}
