import * as prompts from 'prompts';

export interface QuestionPhase {
  conditions: any;
  defaults: any;
  questions: prompts.PromptObject<string>[];
}
