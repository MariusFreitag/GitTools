export enum GitRepositoryMode {
  Default,
  Rebase,
  CherryPick,
  Merge
}
