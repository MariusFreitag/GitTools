export interface RewriteOptions {
  onlyName: string;
  numberCommits: string;
  newName: string;
  newEmail: string;
  newDate: string;
  resetCommitter: boolean;
}
