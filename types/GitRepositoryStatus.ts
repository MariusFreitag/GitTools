import { GitRepositoryMode } from './GitRepositoryMode';

export interface GitRepositoryStatus {
  aheadBy: number;
  behindBy: number;
  isClean: boolean;
  mode: GitRepositoryMode;
}
