export interface LogOptions {
  numberCommits: string;
  json: boolean;
}
