#!/usr/bin/env node

import * as commandExists from 'command-exists';
import * as program from 'commander';
import * as process from 'process';
import { ac } from './common/AdvancedConsole';
import { Bash } from './common/Bash';
import { amendSubcommand } from './subcommands/amend';
import { auditSubcommand } from './subcommands/audit';
import { cleanInternalsSubcommand } from './subcommands/clean-internals';
import { logSubcommand } from './subcommands/log';
import { purgeFileSubcommand } from './subcommands/purge-file';
import { rewriteSubcommand } from './subcommands/rewrite';

// Check if bash is installed
if (!commandExists.sync(Bash.EXECUTABLE_PATH)) {
  ac.error('Bash must be installed and in your PATH!');
  process.exit(1);
}

// Check if git is installed
if (!commandExists.sync('git')) {
  ac.error('Git must be installed and in your PATH!');
  process.exit(1);
}

// Define program interface
program
  .description('Provides useful tools for handling advanced git challenges')
  .version('0.0.1', '-v, --version');
program
  .command('audit [directory]')
  .description('Audit the status, configuration and commit history; only explicitly set options are considered')
  .option('--is-parent', 'consider the given directory as the parent of multiple git repositories')
  .option('--interactive', 'open a command prompt to allow repairing a repository when expections were not met')
  .option('--only-name <name>', 'if set, only commits from this name are considered')
  .option('--allow-github-as-committer', 'allows "GitHub <noreply@github.com>" and similar corporate variations as diverging committer')
  .option('--expect-configured-name <name>', 'author name that should be configured')
  .option('--expect-configured-email <email>', 'author email that should be configured')
  .option('--expect-configured-local-branch <branch>', 'local branch that should be configured')
  .option('--expect-configured-remote-branch <branch>', 'remote branch that should be configured')
  .option('--expect-default-mode', 'expect the git repository to be in the default mode with no rebase, cherry pick or merge ongoing')
  .option('--expect-sync', 'expect the branch to be in sync with it\'s upstream')
  .option('--expect-cleanliness', 'expect the working tree to be clean')
  .option('--expect-author-name <name>', 'author name that should have been used for each commit in the history')
  .option('--expect-author-email <email>', 'author email that should have been used for each commit in the history')
  .option('--expect-author-committer-equality', 'expect that the author and committer dates, names and emails are equal for each commit in the history')
  .action(auditSubcommand);
program
  .command('rewrite [directory]')
  .description('Rewrites the commit history')
  .option('--only-name <name>', 'if set, only commits from this author name are rewritten')
  .option('--number-commits <number>', 'number of commits reachable from HEAD to consider or "all"', '1')
  .option('--new-name <email>', 'author name that will be set on each considered commit')
  .option('--new-email <email>', 'author email that will be set on each considered commit')
  .option('--new-date <date>', 'date that will be set on each considered commit; format should be as in the git log output (example: Sun Jan 1 20:00:00 2010 +0100)')
  .option('--reset-committer', 'reset the committer values to the corresponding author ones')
  .action(rewriteSubcommand);
program
  .command('amend [directory]')
  .description('Interactively amends the most recent commit')
  .action(amendSubcommand);
program
  .command('purge-file [directory]')
  .description('Purges a file from all commits')
  .option('--file <file>', 'name of the file')
  .action(purgeFileSubcommand);
program
  .command('log [directory]')
  .description('Outputs the commit log')
  .option('-n, --number-commits <number>', 'number of commits to output or "all"', '3')
  .option('--json', 'output the log in JSON')
  .action(logSubcommand);
program
  .command('clean-internals [directory]')
  .description('Cleans up the reflog and garbage-collects the repository')
  .action(cleanInternalsSubcommand);

// Define error handling
if (!process.argv.slice(2).length) {
  onInvalidUsage();
}
program.on('command:*', onInvalidUsage);

// Read and parse the command line arguments
program.parse(process.argv);

/**
 * Outputs an error message when the tool usage is invalid
 */
function onInvalidUsage() {
  ac.error(`Invalid usage: ${(program.args || []).join(' ')}\nSee --help for a list of available commands.`);
  process.exit(1);
}
