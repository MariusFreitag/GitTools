import * as path from 'path';
import { FileChange } from '../types/FileChange';
import { GitCommit } from '../types/GitCommit';
import { GitRepositoryBranches } from '../types/GitRepositoryBranches';
import { GitRepositoryMode } from '../types/GitRepositoryMode';
import { GitRepositoryStatus } from '../types/GitRepositoryStatus';
import { Bash } from './Bash';

export class GitRepository {

  private readonly directory: string;

  public constructor(directory: string) {
    this.directory = path.resolve(directory);
  }

  private async executeGitCommand(command: string): Promise<string> {
    return Bash.exec(`git ${command}`, { cwd: this.directory });
  }

  public getName(): string {
    return path.basename(this.directory);
  }

  public getDirectory(): string {
    return this.directory;
  }

  public async isGitRepository(): Promise<boolean> {
    try {
      return (await this.executeGitCommand('rev-parse --is-inside-work-tree')).trim() === 'true';
    } catch (err) {
      return false;
    }
  }

  public async fetch(): Promise<any> {
    return this.executeGitCommand('fetch --all --tags');
  }

  public async getConfig(): Promise<Map<string, string>> {
    const regex = /([\S]*)=([^\n]*)\n/g;

    const rawConfig = await this.executeGitCommand('config --list');
    const config: Map<string, string> = new Map();

    let match: RegExpExecArray | null;
    do {
      match = regex.exec(rawConfig);
      if (match) {
        config.set(match[1], match[2]);
      }
    } while (match);

    return config;
  }

  public async getBranches(): Promise<GitRepositoryBranches> {
    const rawBranchInfo = await this.executeGitCommand('status -sb');

    const regex = /## ([^\.\s]+)(\.\.\.([\S]*?)\s|(\s))?/g;

    const match = regex.exec(rawBranchInfo);

    if (!match) {
      throw new Error('Branches could not be fetched');
    }

    return {
      local: match[1],
      remote: match[3] || ''
    }
  }

  public async getStatus(): Promise<GitRepositoryStatus> {
    const rawStatus = await this.executeGitCommand('status');

    const aheadRegex = /Your branch is ahead of '.*' by ([0-9]*) commit/g;
    const behindRegex = /Your branch is behind '.*' by ([0-9]*) commit/g;
    const divergedRegex = /Your branch and '.*' have diverged,\s*and have ([0-9]*) and ([0-9]*) different commits each, respectively/g;
    const cleanRegex = /nothing to commit, working tree clean/g;
    const rebaseRegex = /interactive rebase in progress;/g;
    const cherryPickRegex = /You are currently cherry-picking commit/g;
    const mergeRegex = /You have unmerged paths./g;

    const aheadMatch = aheadRegex.exec(rawStatus);
    const behindMatch = behindRegex.exec(rawStatus);
    const divergedMatch = divergedRegex.exec(rawStatus);
    const cleanMatch = cleanRegex.exec(rawStatus);
    const rebaseMatch = rebaseRegex.exec(rawStatus);
    const cherryPickMatch = cherryPickRegex.exec(rawStatus);
    const mergeMatch = mergeRegex.exec(rawStatus);

    return {
      aheadBy: divergedMatch ? Number(divergedMatch[1]) : (aheadMatch ? Number(aheadMatch[1]) : 0),
      behindBy: divergedMatch ? Number(divergedMatch[2]) : (behindMatch ? Number(behindMatch[1]) : 0),
      isClean: !!cleanMatch,
      mode: rebaseMatch ? GitRepositoryMode.Rebase : (cherryPickMatch ? GitRepositoryMode.CherryPick : (mergeMatch ? GitRepositoryMode.Merge : GitRepositoryMode.Default))
    };
  }

  public async getLog(numberCommits: number = -1): Promise<GitCommit[]> {
    const commitFormat: GitCommit = {
      "hash": "%H",
      "shortHash": "%h",
      "tree": "%T",
      "shortTree": "%t",
      "parent": "%P",
      "shortParent": "%p",
      "refs": "%D",
      "encoding": "%e",
      "subject": "%s",
      "sanitizedSubjectLine": "%f",
      "body": "%b",
      "commitNotes": "",
      "verificationFlag": "%G?",
      "signer": "%GS",
      "signerKey": "%GK",
      "author": {
        "name": "%aN",
        "email": "%aE",
        "date": "%aD"
      },
      "committer": {
        "name": "%cN",
        "email": "%cE",
        "date": "%cD"
      }
    };

    const escapeString = Math.random().toString().substring(2, 5) + Math.random().toString().substring(2, 5) + '/-_-/';
    const escapedCommitFormat = JSON.stringify(commitFormat).replace(/"/g, escapeString);
    const escapedLogOutput = (await this.executeGitCommand(`log --max-count ${numberCommits} --pretty=format:"${escapedCommitFormat},"`));
    const logOutput = escapedLogOutput.replace(/\s+/g, ' ').replace(/\\/g, '\\\\').replace(/"/g, '\\"').replace(new RegExp(escapeString, 'g'), '"').trim();
    const jsonString = `[${logOutput.substring(0, logOutput.length - 1)}]`;

    return JSON.parse(jsonString);
  }

  public async getFileChanges(commit: GitCommit): Promise<FileChange[]> {
    const rawLogOutput = await this.executeGitCommand(`show --stat ${commit.hash}`);

    const regex = /^ ([\S]*( => [\S]*)?)*\s*\|\s*(([0-9]*)|Bin) ?([+-]*)$/gm;

    const fileChanges: FileChange[] = [];

    let match: RegExpExecArray | null;
    do {
      match = regex.exec(rawLogOutput);

      if (match) {
        const changedLines = match[4] ? Number(match[4]) : 0;
        const additionPortion = (match[5].match(/\+/g) || []).length;
        const removalPortion = (match[5].match(/\-/g) || []).length;

        fileChanges.push({
          filename: match[1],
          changedLines,
          additionPortion,
          removalPortion,
          isBinary: !match[4]
        });
      }
    } while (match);

    return fileChanges;
  }
}
