import { expect } from 'chai';
import 'mocha';
import { EOL } from 'os';
import { fake } from 'sinon';
import { BackgroundColor } from '../types/BackgroundColor';
import { ColorModifier } from '../types/ColorModifier';
import { ForegroundColor } from '../types/ForegroundColor';
import { AdvancedConsole } from './AdvancedConsole';

const TEST_OUTPUT = 'TEST_OUTPUT';

describe('Advanced Console', () => {
  it('should be a function', () => {
    expect(AdvancedConsole).to.be.a('function');
  });
});

describe('Instant Advanced Console', () => {
  it('should be created', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    expect(instance.instant).to.exist;
  });

  it('should log instantly', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.log(TEST_OUTPUT);
    expect(mockWriter.write.calledOnceWith(TEST_OUTPUT)).to.to.true;
  });

  it('should add a tab prefix', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.tab.log(TEST_OUTPUT);
    expect(mockWriter.write.calledOnceWith('  ' + TEST_OUTPUT)).to.to.true;
  });

  it('should add a new line prefix', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.newLine.log(TEST_OUTPUT);
    expect(mockWriter.write.calledOnceWith(EOL + TEST_OUTPUT)).to.to.true;
  });

  it('should add no color properties', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.color(TEST_OUTPUT, null, null, null);
    expect(mockWriter.write.calledOnceWith(TEST_OUTPUT + ColorModifier.Reset)).to.to.true;
  });

  it('should add a foreground color', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.color(TEST_OUTPUT, ForegroundColor.Red, null, null);
    expect(mockWriter.write.calledOnceWith(ForegroundColor.Red + TEST_OUTPUT + ColorModifier.Reset)).to.to.true;
  });

  it('should add a background color', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.color(TEST_OUTPUT, null, BackgroundColor.Red, null);
    expect(mockWriter.write.calledOnceWith(BackgroundColor.Red + TEST_OUTPUT + ColorModifier.Reset)).to.to.true;
  });

  it('should add a color modifier', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.color(TEST_OUTPUT, null, null, ColorModifier.Bright);
    expect(mockWriter.write.calledOnceWith(ColorModifier.Bright + TEST_OUTPUT + ColorModifier.Reset)).to.to.true;
  });

  it('should add a foreground color and a background color and a color modifier', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.color(TEST_OUTPUT, ForegroundColor.Red, BackgroundColor.Red, ColorModifier.Bright);
    expect(mockWriter.write.calledOnceWith(ForegroundColor.Red + BackgroundColor.Red + ColorModifier.Bright + TEST_OUTPUT + ColorModifier.Reset)).to.to.true;
  });

  it('should log brightly', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.bright(TEST_OUTPUT);
    const [actual] = mockWriter.write.getCall(0).args;
    expect(actual).to.match(RegExp('.*' + TEST_OUTPUT + escapeRegExp(ColorModifier.Reset)))
  });

  it('should log as heading', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.heading(TEST_OUTPUT);
    const [actual] = mockWriter.write.getCall(0).args;
    expect(actual).to.match(RegExp('.*' + TEST_OUTPUT + escapeRegExp(ColorModifier.Reset)))
  });

  it('should log emphasized', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.emphasis(TEST_OUTPUT);
    const [actual] = mockWriter.write.getCall(0).args;
    expect(actual).to.match(RegExp('.*' + TEST_OUTPUT + escapeRegExp(ColorModifier.Reset)))
  });

  it('should log as success', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.success(TEST_OUTPUT);
    const [actual] = mockWriter.write.getCall(0).args;
    expect(actual).to.match(RegExp('.*' + TEST_OUTPUT + escapeRegExp(ColorModifier.Reset)))
  });

  it('should log as warning', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.warning(TEST_OUTPUT);
    const [actual] = mockWriter.write.getCall(0).args;
    expect(actual).to.match(RegExp('.*' + TEST_OUTPUT + escapeRegExp(ColorModifier.Reset)))
  });

  it('should log as error', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.instant.error(TEST_OUTPUT);
    const [actual] = mockWriter.write.getCall(0).args;
    expect(actual).to.match(RegExp('.*' + TEST_OUTPUT + escapeRegExp(ColorModifier.Reset)))
  });
});


describe('Buffered Advanced Console', () => {
  it('should be created', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    expect(instance.buffered).to.exist;
  });

  it('should not log instantly', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.buffered.log(TEST_OUTPUT);
    expect(mockWriter.write.notCalled).to.be.true;
  });

  it('should log after flush is called', () => {
    const mockWriter = { write: fake() }
    const instance = new AdvancedConsole(mockWriter, false, '', '');
    instance.buffered.log(TEST_OUTPUT).log(TEST_OUTPUT).flush();
    expect(mockWriter.write.calledOnceWith(TEST_OUTPUT + TEST_OUTPUT)).to.be.true;
  });
});

function escapeRegExp(string: string): string {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
