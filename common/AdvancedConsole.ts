import { EOL } from 'os';
import { BackgroundColor } from '../types/BackgroundColor';
import { ColorModifier } from '../types/ColorModifier';
import { ForegroundColor } from '../types/ForegroundColor';
import { Writer } from '../types/Writer';

export class AdvancedConsole {
  constructor(private readonly writer: Writer, private readonly doBuffer: boolean, private readonly prefix: string, private readonly buffer: string) { }

  get instant(): AdvancedConsole {
    return new AdvancedConsole(this.writer, false, this.prefix, this.buffer);
  }

  get buffered(): AdvancedConsole {
    return new AdvancedConsole(this.writer, true, this.prefix, this.buffer);
  }

  get tab(): AdvancedConsole {
    return new AdvancedConsole(this.writer, this.doBuffer, '  ' + this.prefix, this.buffer);
  }

  get newLine(): AdvancedConsole {
    return new AdvancedConsole(this.writer, this.doBuffer, EOL + this.prefix, this.buffer);
  }

  public log(text: string): AdvancedConsole {
    const enrichedInstance = new AdvancedConsole(this.writer, this.doBuffer, this.prefix, this.buffer + text);
    return this.doBuffer ? enrichedInstance : enrichedInstance.flush();
  }

  public color(text: string, foregroundColor: ForegroundColor | null, backgroundColor: BackgroundColor | null, colorModifier: ColorModifier | null): AdvancedConsole {
    return this.log((foregroundColor || '') + (backgroundColor || '') + (colorModifier || '') + text + ColorModifier.Reset);
  }

  public bright(text: string): AdvancedConsole {
    return this.color(text, ForegroundColor.White, null, ColorModifier.Bright);
  }

  public heading(text: string): AdvancedConsole {
    return this.color(text, ForegroundColor.White, null, ColorModifier.Bright);
  }

  public emphasis(text: string): AdvancedConsole {
    return this.color(text, ForegroundColor.Yellow, null, ColorModifier.Bright);
  }

  public success(text: string): AdvancedConsole {
    return this.color(text, ForegroundColor.Green, null, ColorModifier.Bright);
  }

  public warning(text: string): AdvancedConsole {
    return this.color(text, ForegroundColor.Magenta, null, ColorModifier.Bright);
  }

  public error(text: string): AdvancedConsole {
    return this.color(text, ForegroundColor.Red, null, ColorModifier.Bright);
  }

  public flush(): AdvancedConsole {
    this.writer.write(this.prefix + this.buffer);
    return new AdvancedConsole(this.writer, this.doBuffer, '', '');
  }
}

const ac = new AdvancedConsole({
  write: (text: string) => console.log(text)
}, false, '', '');

export { ac };

