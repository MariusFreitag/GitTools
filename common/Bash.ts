import { ExecOptions, spawn } from 'child_process';
import { ac } from "./AdvancedConsole";

export class Bash {
  public static readonly EXECUTABLE_PATH = 'bash';

  public static async exec(command: string, options: ExecOptions, log: boolean = false): Promise<string> {
    return new Promise((resolve, reject) => {
      let output = '';
      const childProcess = spawn(Bash.EXECUTABLE_PATH, ['-c', command], options);
      childProcess.on('error', reject)
      childProcess.on('exit', () => resolve(output));
      childProcess.stdout.on('data', (chunk) => {
        output += chunk;
        log && ac.log(chunk.toString());
      });
      childProcess.stderr.on('data', (chunk) => {
        output += chunk;
        log && ac.warning(chunk.toString());
      });
    });
  }
}
