import { ac } from '../common/AdvancedConsole';
import { GitRepository } from '../common/GitRepository';
import { ColorModifier } from '../types/ColorModifier';
import { FileChange } from '../types/FileChange';
import { ForegroundColor } from '../types/ForegroundColor';
import { GitCommit } from '../types/GitCommit';
import { LogOptions } from '../types/LogOptions';

export async function logSubcommand(directory: string, options: LogOptions): Promise<void> {
  if (options.numberCommits !== 'all' && (isNaN(Number(options.numberCommits)) || Number(options.numberCommits) < 1)) {
    ac.error('Number of commits is not specified as a number or "all"!');
    process.exit(1);
  }

  const gitRepository = new GitRepository(directory || '.');

  if (!(await gitRepository.isGitRepository())) {
    ac.error(`Not a git repository: ${gitRepository.getName()}`);
    process.exit(1);
  }

  const commits = await gitRepository.getLog(isNaN(Number(options.numberCommits)) ? -1 : Number(options.numberCommits));

  const fileChangePromises: Map<GitCommit, Promise<FileChange[]>> = new Map();
  for (let i = 0; i < commits.length; i += 1) {
    fileChangePromises.set(commits[i], gitRepository.getFileChanges(commits[i]));
  }

  if (options.json) {
    ac.log(JSON.stringify(commits, null, 2));
  } else {
    for (let i = 0; i < Array.from(fileChangePromises.entries()).length; i += 1) {
      const [commit, FileChangePromise] = Array.from(fileChangePromises.entries()).reverse()[i];
      const fileChanges = await FileChangePromise;

      ac.buffered
        .log(`HEAD~${commits.length - 1 - i}: `)
        .emphasis(commit.subject)
        .flush();

      ac.tab.bright(commit.hash);

      const tags = commit.refs.split(', ').filter(ref => ref.startsWith('tag: ')).map(ref => ref.substring('tag: '.length));
      const headBranch = commit.refs.split(', ').filter(ref => ref && ref.startsWith('HEAD -> ')).map(ref => ref.substring('HEAD -> '.length))[0];
      const branches = commit.refs.split(', ').filter(ref => ref && !ref.startsWith('tag: ') && !ref.startsWith('HEAD -> '));

      if (headBranch || branches.length > 0) {
        ac.tab.buffered
          .log('Branches: ')
          .emphasis(headBranch ? headBranch : '')
          .bright(headBranch && branches.length > 0 ? ', ' : '')
          .color(branches.join(', '), ForegroundColor.Cyan, null, ColorModifier.Bright)
          .flush();
      }

      if (tags.length > 0) {
        ac.tab.buffered
          .log('Tags: ')
          .color(tags.join(', '), ForegroundColor.Cyan, null, ColorModifier.Bright)
          .flush();
      }


      ac.tab.buffered
        .log('Author: ')
        .bright(`${commit.author.name} <${commit.author.email}>`)
        .flush();

      if (commit.author.name !== commit.committer.name || commit.author.email !== commit.committer.email) {
        ac.tab.buffered
          .log('Committer: ')
          .warning(`${commit.committer.name} <${commit.committer.email}>`)
          .flush();
      }

      ac.tab.buffered
        .log('Author Date: ')
        .bright(String(commit.author.date))
        .flush();

      if (commit.author.date !== commit.committer.date) {
        ac.tab.buffered
          .log('Comitter Date: ')
          .warning(String(commit.author.date))
          .flush();
      }

      const maxFilenameLength = fileChanges.reduce((acc, cur) => Math.max(acc, cur.filename.length), 0);
      const maxChangedLines = fileChanges.reduce((acc, cur) => Math.max(acc, cur.changedLines), 0);

      fileChanges.forEach((fileChange) => {
        const filenameGap = maxFilenameLength - fileChange.filename.length + 1;
        const changedLinesGap = String(maxChangedLines).length - String(fileChange.changedLines).length + 1;

        const builder = ac.tab.tab.buffered
          .bright(fileChange.filename)
          .log(Array(filenameGap + 1).join(' '));

        if (fileChange.changedLines) {
          builder
            .log('Changed lines: ')
            .bright(String(fileChange.changedLines))
            .log(Array(changedLinesGap + 1).join(' '))
            .log('Ratio: ')
            .color(Array(fileChange.additionPortion + 1).join('+'), ForegroundColor.Green, null, ColorModifier.Bright)
            .color(Array(fileChange.removalPortion + 1).join('-'), ForegroundColor.Red, null, ColorModifier.Bright);
        }

        if (fileChange.isBinary) {
          builder.log('Binary');
        }

        builder.flush();
      });

      ac.log('');
    };
  }

  process.exit(0);
}
