import { exec, ExecException } from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import { promisify } from 'util';
import { ac } from '../common/AdvancedConsole';
import { GitRepository } from '../common/GitRepository';
import { AuditOptions } from '../types/AuditOptions';
import { GitRepositoryMode } from '../types/GitRepositoryMode';

export async function auditSubcommand(directory: string, options: AuditOptions): Promise<void> {
  const possibleExpectations = ['expectConfiguredName', 'expectConfiguredEmail', 'expectConfiguredLocalBranch', 'expectConfiguredRemoteBranch', 'expectSync', 'expectDefaultMode', 'expectCleanliness', 'expectAuthorName', 'expectAuthorEmail', 'expectAuthorCommitterEquality'];
  const expectationCount = `[with ${possibleExpectations.reduce((acc, cur) => acc + ((options as any)[cur] ? 1 : 0), 0)}/${possibleExpectations.length} expectations]`;

  let directories = [directory || '.'];

  if (options.isParent) {
    directories = (await promisify(fs.readdir)(directories[0]))
      .map(child => path.join(directories[0], child));
  }

  const fetchPromises: Map<GitRepository, Promise<any>> = new Map();
  for (let directory of directories) {
    const gitRepository = new GitRepository(directory);

    if (!(await gitRepository.isGitRepository())) {
      ac.log(`Ignoring non-git file or directory ${gitRepository.getName()}`);
      continue;
    }

    if (options.expectSync) {
      fetchPromises.set(gitRepository, gitRepository.fetch().catch(err => err));
    } else {
      fetchPromises.set(gitRepository, Promise.resolve());
    }
  }

  let hadError = false;
  for (let [gitRepository, fetchPromise] of Array.from(fetchPromises.entries())) {
    while (true) {
      ac.heading(`Auditing ${gitRepository.getName()} ${expectationCount}`);

      const error = await fetchPromise;

      if (error instanceof Error) {
        ac.tab.error(`Fetch failed: ${error.message.replace(/[\r\n]{1,2}/g, ' ')}`);
      }

      const config = await gitRepository.getConfig();
      const branches = await gitRepository.getBranches();
      const status = await gitRepository.getStatus();
      const log = await gitRepository.getLog();

      let expectationsMet = true;

      if ((options.expectConfiguredName && config.get('user.name') !== options.expectConfiguredName)
        || (options.expectConfiguredEmail && config.get('user.email') !== options.expectConfiguredEmail)) {
        ac.buffered
          .tab
          .log(`Configuration: `)
          .emphasis(`${config.get('user.name')} <${config.get('user.email')}>`)
          .log(' <=> ')
          .emphasis(`${options.expectConfiguredName} <${options.expectConfiguredEmail}>`)
          .flush();
        expectationsMet = false;
      }

      if ((options.expectConfiguredLocalBranch && branches.local !== options.expectConfiguredLocalBranch)
        || (options.expectConfiguredRemoteBranch && branches.remote !== options.expectConfiguredRemoteBranch)) {
        ac.buffered
          .tab
          .log('Configuration: ')
          .emphasis(`${branches.local} -> ${branches.remote}`)
          .log(' <=> ')
          .emphasis(`${options.expectConfiguredLocalBranch} -> ${options.expectConfiguredRemoteBranch}`)
          .flush();
        expectationsMet = false;
      }

      if (options.expectDefaultMode && status.mode !== GitRepositoryMode.Default) {
        ac.buffered
          .tab
          .log('Repository is in mode ')
          .emphasis(GitRepositoryMode[status.mode])
          .flush();
        expectationsMet = false;
      }

      if (options.expectSync && status.aheadBy > 0) {
        ac.buffered
          .tab
          .log('Ahead by ')
          .emphasis(String(status.aheadBy))
          .flush();
        expectationsMet = false;
      }

      if (options.expectSync && status.behindBy > 0) {
        ac.buffered
          .tab
          .log('Behind by ')
          .emphasis(String(status.behindBy))
          .flush();
        expectationsMet = false;
      }

      if (options.expectCleanliness && !status.isClean) {
        ac.tab.log('Not clean');
        expectationsMet = false;
      }

      log
        .filter((commit) => !options.onlyName || commit.author.name === options.onlyName)
        .forEach((commit) => {
          if (options.expectAuthorName && commit.author.name !== options.expectAuthorName) {
            ac.buffered
              .tab
              .log('Commit ')
              .emphasis(commit.subject)
              .log(': ')
              .log('Author name ')
              .emphasis(commit.author.name)
              .log(' <=> ')
              .emphasis(options.expectAuthorName)
              .flush();
            expectationsMet = false;
          }
          if (options.expectAuthorEmail && commit.author.email !== options.expectAuthorEmail) {
            ac.buffered
              .tab
              .log('Commit ')
              .emphasis(commit.subject)
              .log(': ')
              .log('Author email ')
              .emphasis(commit.author.email)
              .log(' <=> ')
              .emphasis(options.expectAuthorEmail)
              .flush();
            expectationsMet = false;
          }
          if (options.expectAuthorCommitterEquality) {
            const ignoreNameOrEmail = options.allowGithubAsCommitter && commit.committer.name.match(/GitHub( Enterprise)?/) && commit.committer.email.match(/(noreply@github\.com)|(noreply\+github@.*\..*)/);

            if (commit.author.date !== commit.committer.date
              || (!ignoreNameOrEmail && commit.author.name !== commit.committer.name)
              || (!ignoreNameOrEmail && commit.author.email !== commit.committer.email)) {
              ac.buffered
                .tab
                .log('Commit ')
                .emphasis(commit.subject)
                .log(': ')
                .log('Author ')
                .emphasis(`${commit.author.name} <${commit.author.email}> (${commit.author.date})`)
                .log(' <=> ')
                .log('Committer ')
                .emphasis(`${commit.committer.name} <${commit.committer.email}> (${commit.committer.date})`)
                .flush();
              expectationsMet = false;
            }
          }
        });

      if (expectationsMet) {
        break;
      }

      ac.tab.error('Expectations not met');

      if (options.interactive) {
        await new Promise((resolve, reject) => {
          exec('start cmd /K git status', { cwd: gitRepository.getDirectory() }, (error: ExecException | null, stdout: string, stderr: string) => {
            if (error) {
              return reject(error.message);
            }
            if (stderr) {
              return reject(stderr);
            }
            resolve(stdout);
          });
        });
      } else {
        hadError = true;
        break;
      }
    }
  }

  if (hadError) {
    if (options.isParent) {
      ac.error('Done with unmet expectations');
    }
    process.exit(1);
  } else {
    ac.success('Done');
    process.exit(0);
  }
}
