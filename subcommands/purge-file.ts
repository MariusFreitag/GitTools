import { ac } from '../common/AdvancedConsole';
import { Bash } from '../common/Bash';
import { GitRepository } from '../common/GitRepository';
import { PurgeFileOptions } from '../types/PurgeFileOptions';

export async function purgeFileSubcommand(directory: string, options: PurgeFileOptions): Promise<void> {
  if (!options.file) {
    ac.error('No file specified!');
    process.exit(1);
  }

  const gitRepository = new GitRepository(directory || '.');

  if (!(await gitRepository.isGitRepository())) {
    ac.error(`Not a git repository: ${gitRepository.getName()}`);
    process.exit(1);
  }

  ac.heading(`Purging "${options.file}" from ${gitRepository.getName()}`);

  const command = `git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch "${options.file}"\' --prune-empty --tag-name-filter cat -- --all`;
  ac.log('Git command: ' + command);

  try {
    await Bash.exec(command, { cwd: gitRepository.getDirectory() }, true);
  } catch (err) {
    ac.error('Command execution failed');
    process.exit(1);
  }

  ac.success('Done');
  process.exit(0);
}
