import * as prompts from 'prompts';
import { ac } from '../common/AdvancedConsole';
import { Bash } from '../common/Bash';
import { GitRepository } from '../common/GitRepository';
import { AmendAction } from '../types/AmendAction';
import { GitCommit } from '../types/GitCommit';
import { GitRepositoryMode } from '../types/GitRepositoryMode';
import { GitRepositoryStatus } from '../types/GitRepositoryStatus';
import { QuestionPhase } from '../types/QuestionPhase';

export async function amendSubcommand(directory: string): Promise<void> {
  const gitRepository = new GitRepository(directory || '.');

  if (!(await gitRepository.isGitRepository())) {
    ac.error(`Not a git repository: ${gitRepository.getName()}`);
    process.exit(1);
  }

  const status = await gitRepository.getStatus();
  const [lastCommit, previousCommit] = await gitRepository.getLog(2);

  ac.heading(`Amending '${lastCommit.subject}' from ${gitRepository.getName()}`);

  const questionPhases = getQuestionPhases(lastCommit, previousCommit, status);

  let answers: any = {};
  for (const questionPhase of questionPhases) {
    const conditionsMet = Object.keys(questionPhase.conditions).reduce((acc, curr) => acc && answers[curr] === questionPhase.conditions[curr], true);

    if (conditionsMet) {
      answers = Object.assign(answers, await prompts(questionPhase.questions, { onCancel }));
    } else {
      answers = Object.assign(answers, questionPhase.defaults);
    }
  }

  let command = '';

  if (answers.action !== AmendAction[AmendAction.Nothing]) {

    if (answers.action === AmendAction[AmendAction.Change]) {
      command = `export MESSAGE="${answers.message}"; `;
      command += `export GIT_AUTHOR_DATE="${answers.authorDate}"; `;
      command += `export GIT_AUTHOR_NAME="${answers.authorName}"; `;
      command += `export GIT_AUTHOR_EMAIL="${answers.authorEmail}"; `;
      command += `export GIT_COMMITTER_DATE="${answers.resetCommitter ? answers.authorDate : answers.committerDate}"; `;
      command += `export GIT_COMMITTER_NAME="${answers.resetCommitter ? answers.authorName : answers.committerName}"; `;
      command += `export GIT_COMMITTER_EMAIL="${answers.resetCommitter ? answers.authorEmail : answers.committerEmail}"; `;
      command += `git commit --amend --allow-empty -m "$MESSAGE" --date "$GIT_AUTHOR_DATE" --author "$GIT_AUTHOR_NAME <$GIT_AUTHOR_EMAIL>"`;
      command += !answers.addStagedFiles ? ' --only' : '';
      command += answers.continueRebase ? ' && git rebase --continue' : '';
    }

    if (answers.action === AmendAction[AmendAction.Remove]) {
      if (!answers.removeConfirmed) {
        return onCancel();
      }

      command = 'git reset'
      command += ' && git reset HEAD~ --hard';
    }

    if (answers.action === AmendAction[AmendAction.Squash]) {
      if (!answers.squashConfirmed) {
        return onCancel();
      }

      command = `export MESSAGE="${previousCommit.subject}"; `;
      command += `export GIT_AUTHOR_DATE="${previousCommit.author.date}"; `;
      command += `export GIT_AUTHOR_NAME="${previousCommit.author.name}"; `;
      command += `export GIT_AUTHOR_EMAIL="${previousCommit.author.email}"; `;
      if (!answers.updateCommitter) {
        command += `export GIT_COMMITTER_DATE="${previousCommit.committer.date}"; `;
        command += `export GIT_COMMITTER_NAME="${previousCommit.committer.name}"; `;
        command += `export GIT_COMMITTER_EMAIL="${previousCommit.committer.email}"; `;
      }
      command += 'git reset'
      command += ' && git reset HEAD~ --soft';
      command += ' && git commit --amend --allow-empty -m "$MESSAGE" --date "$GIT_AUTHOR_DATE" --author "$GIT_AUTHOR_NAME <$GIT_AUTHOR_EMAIL>"';
    }

    ac.log('Git command: ' + command);

    try {
      await Bash.exec(command, { cwd: gitRepository.getDirectory() }, true);
    } catch (err) {
      ac.error('Command execution failed');
      process.exit(1);
    }
  }

  const [updatedLastCommit] = await gitRepository.getLog(1);
  if (updatedLastCommit.hash === lastCommit.hash) {
    ac.emphasis('Commit unchanged');
  } else {
    ac.emphasis(`Commit updated: ${updatedLastCommit.hash} -> ${lastCommit.hash}`);
  }

  ac.success('Done');
  process.exit(0);
}

/**
 * Validates a date according to https://tools.ietf.org/html/rfc2822 (simplified)
 */
function validateDate(value: string): boolean {
  return !!/^\s*[A-Z][a-z]{2}(,)? ([A-Z][a-z]{2}|[0-9]{1,2}) ([A-Z][a-z]{2}|[0-9]{1,2}) ([0-9]{2}:[0-9]{2}:[0-9]{2}|[0-9]{4}) ([0-9]{2}:[0-9]{2}:[0-9]{2}|[0-9]{4}) (((\+|-)[0-9]{4})|UT|"GMT")?\s*$/.exec(value);
}

/**
 * Cancels the questionnaire
 */
function onCancel(): void {
  ac.error('Aborted');
  process.exit(1);
}

/**
 * Creates and returns phases for the questionnaire
 */
function getQuestionPhases(lastCommit: GitCommit, previousCommit: GitCommit, gitStatus: GitRepositoryStatus): QuestionPhase[] {
  return [
    // Select action
    {
      conditions: {},
      defaults: {
        action: AmendAction[AmendAction.Nothing]
      },
      questions: [
        {
          type: 'select',
          name: 'action',
          message: 'What do you want to do with this commit?',
          choices: [
            { title: 'Nothing', value: AmendAction[AmendAction.Nothing], },
            { title: 'Change', value: AmendAction[AmendAction.Change] },
            { title: `Squash into previous (${previousCommit ? previousCommit.subject : 'not available'})`, value: AmendAction[AmendAction.Squash], disabled: !previousCommit } as prompts.Choice,
            { title: 'Remove', value: AmendAction[AmendAction.Remove] }
          ],
          initial: 0
        }
      ]
    },
    // Set commit message and select whether to change additional values
    {
      conditions: {
        action: AmendAction[AmendAction.Change]
      },
      defaults: {
        message: lastCommit.subject,
        changeAdditionalValues: false
      },
      questions: [
        {
          type: 'text',
          name: 'message',
          message: 'Commit message',
          initial: lastCommit.subject
        },
        {
          type: 'confirm',
          name: 'changeAdditionalValues',
          message: 'Do you want to change additional values?',
          initial: true
        }
      ]
    },
    // Set author values and select whether to reset committer values
    {
      conditions: {
        action: AmendAction[AmendAction.Change],
        changeAdditionalValues: true
      },
      defaults: {
        authorDate: lastCommit.author.date,
        authorName: lastCommit.author.name,
        authorEmail: lastCommit.author.email,
        resetCommitter: false
      },
      questions: [
        {
          type: 'text',
          name: 'authorDate',
          message: 'Author date',
          initial: lastCommit.author.date,
          validate: validateDate
        },
        {
          type: 'text',
          name: 'authorName',
          message: 'Author name',
          initial: lastCommit.author.name
        },
        {
          type: 'text',
          name: 'authorEmail',
          message: 'Author email',
          initial: lastCommit.author.email
        },
        {
          type: 'confirm',
          name: 'resetCommitter',
          message: 'Do you want to set the committer values to the author ones?',
          initial: false
        }
      ]
    },
    // Set committer values
    {
      conditions: {
        action: AmendAction[AmendAction.Change],
        changeAdditionalValues: true,
        resetCommitter: false
      },
      defaults: {
        committerDate: lastCommit.committer.date,
        committerName: lastCommit.committer.name,
        committerEmail: lastCommit.committer.email,
      },
      questions: [
        {
          type: 'text',
          name: 'committerDate',
          message: 'Committer date',
          initial: lastCommit.committer.date,
          validate: validateDate
        },
        {
          type: 'text',
          name: 'committerName',
          message: 'Committer name',
          initial: lastCommit.committer.name
        },
        {
          type: 'text',
          name: 'committerEmail',
          message: 'Committer email',
          initial: lastCommit.committer.email
        }
      ]
    },
    // Select whether to add stages files and continue the rebase if one is in ongoing
    {
      conditions: {
        action: AmendAction[AmendAction.Change]
      },
      defaults: {
        addStagedFiles: false,
        continueRebase: false
      },
      questions: [
        {
          type: 'confirm',
          name: 'addStagedFiles',
          message: 'Do you want to add staged files to this commit?',
          initial: false
        },
        {
          type: gitStatus.mode === GitRepositoryMode.Rebase ? 'confirm' : null,
          name: 'continueRebase',
          message: 'You currently have a rebase ongoing. Do you want to continue to the next commit?',
          initial: false
        }
      ]
    },
    // Confirm squash action and select whether to reset committer values
    {
      conditions: {
        action: AmendAction[AmendAction.Squash]
      },
      defaults: {
        squashConfirmed: false,
        updateCommitter: true
      },
      questions: [
        {
          type: 'confirm',
          name: 'updateCommitter',
          message: 'Do you want to update the committer values of the previous commit?',
          initial: true
        },
        {
          type: 'confirm',
          name: 'squashConfirmed',
          message: 'Are you sure? This will remove the current commit and modify the previous one',
          initial: false
        }
      ]
    },
    // Confirm remove action
    {
      conditions: {
        action: AmendAction[AmendAction.Remove]
      },
      defaults: {
        removeConfirmed: false
      },
      questions: [
        {
          type: 'confirm',
          name: 'removeConfirmed',
          message: 'Are you sure? This will remove the current commit and all changes included, also the ones in your working tree',
          initial: false
        }
      ]
    }
  ];
}
