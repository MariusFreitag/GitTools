import { ac } from '../common/AdvancedConsole';
import { Bash } from '../common/Bash';
import { GitRepository } from '../common/GitRepository';
import { RewriteOptions } from '../types/RewriteOptions';

export async function rewriteSubcommand(directory: string, options: RewriteOptions): Promise<void> {
  if (options.numberCommits !== 'all' && (isNaN(Number(options.numberCommits)) || Number(options.numberCommits) < 1)) {
    ac.error('Number of commits is not specified as a number or "all"!');
    process.exit(1);
  }

  const gitRepository = new GitRepository(directory || '.');

  if (!(await gitRepository.isGitRepository())) {
    ac.error(`Not a git repository: ${gitRepository.getName()}`);
    process.exit(1);
  }

  ac.heading('Rewriting ' + gitRepository.getName());

  const command = await generateCommand(options);
  ac.log('Git command: ' + command);

  try {
    await Bash.exec(command, {
      cwd: gitRepository.getDirectory(),
      env: {
        ...process.env,
        FILTER_BRANCH_SQUELCH_WARNING: '1'
      }
    }, true);
  } catch (err) {
    ac.error('Command execution failed');
    process.exit(1);
  }

  ac.success('Done');
  process.exit(0);
}

/**
 * Generates the bash command to rewrite the git history
 */
async function generateCommand(options: RewriteOptions): Promise<string> {
  let command = '';

  if (options.newDate) {
    command += `export GIT_AUTHOR_DATE="${options.newDate}"; `;
  }

  if (options.newName) {
    command += `export GIT_AUTHOR_NAME="${options.newName}"; `;
  }

  if (options.newEmail) {
    command += `export GIT_AUTHOR_EMAIL="${options.newEmail}"; `;
  }

  if (options.resetCommitter) {
    command += `export GIT_COMMITTER_DATE="${options.newDate ? options.newDate : "$GIT_AUTHOR_DATE"}"; `;
    command += `export GIT_COMMITTER_NAME="${options.newName ? options.newName : "$GIT_AUTHOR_NAME"}"; `;
    command += `export GIT_COMMITTER_EMAIL="${options.newEmail ? options.newEmail : "$GIT_AUTHOR_EMAIL"}"; `;
  }

  command = command ? command.substring(0, command.length - 2) : '';

  if (options.onlyName) {
    command = `if [ "$GIT_AUTHOR_NAME" = "${options.onlyName}" ]; then ${command}; fi`;
  }

  command = `git filter-branch --force --env-filter \'${command}\' --tag-name-filter cat -- --branches --tags HEAD`;

  if (options.numberCommits !== 'all') {
    command += `~${Number(options.numberCommits)}..HEAD`;
  }

  return command;
}
