import { ac } from '../common/AdvancedConsole';
import { Bash } from '../common/Bash';
import { GitRepository } from '../common/GitRepository';

export async function cleanInternalsSubcommand(directory: string): Promise<void> {
  const gitRepository = new GitRepository(directory || '.');

  if (!(await gitRepository.isGitRepository())) {
    ac.error(`Not a git repository: ${gitRepository.getName()}`);
    process.exit(1);
  }

  ac.heading(`Cleaning up the internals of ${gitRepository.getName()}`);

  const command = 'git reflog expire --expire=now --all && git gc --prune=now --aggressive';
  ac.log('Git command: ' + command);

  try {
    await Bash.exec(command, { cwd: gitRepository.getDirectory() }, true);
  } catch (err) {
    ac.error('Command execution failed');
    process.exit(1);
  }

  ac.success('Done');
  process.exit(0);
}
